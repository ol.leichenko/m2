package hackerrank;

import java.util.Scanner;

public class StringReverse {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String A = sc.next();

        char[] a = A.toCharArray();
        int start = 0;
        int end = a.length-1;
        boolean isPalindrome = true;

        while (start < end){
            if (a[start] != a[end]){
                isPalindrome = false;
            }
            else{
            }
            start++;
            end--;
        }
        System.out.println(isPalindrome ? "Yes" : "No");
    }
}
