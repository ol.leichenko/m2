package basic.contest2;
import java.util.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MyArrayListSort {

    public static void main(String a[]){

        List<Empl> list = new ArrayList<Empl>();
        list.add(new Empl("0",100));
        list.add(new Empl("1",51));
        list.add(new Empl("2",150));
        list.add(new Empl("3",2400));
        Collections.sort(list,new MySalaryComp());
        System.out.println("Sorted list entries: ");
        for(Empl e:list){
            System.out.println(e.getSalary() + " " + e.getId());
        }
    }
}

class MySalaryComp implements Comparator<Empl>{

    @Override
    public int compare(Empl e1, Empl e2) {
        if(e1.getSalary() < e2.getSalary()){
            return 1;
        } else {
            return -1;
        }
    }
}

class Empl{
    private String id;
    //private String name;
    private int salary;

    public Empl(String idn, int salaryn){
        this.id = idn;
        this.salary = salaryn;
    }
    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id = id;
    }
    /*public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }*/
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    /*public String toString(){
        return "Name: "+this.name+"-- Salary: "+this.salary;
    }*/
}