package basic.contest2;

import java.util.HashSet;
import java.util.Scanner;

public class Pairs{

    static void amountPairs(int arr[],int sum)
    {
        HashSet<Integer> s = new HashSet<Integer>();
        int count = 0;
        for (int i = 0; i < arr.length; ++i)
        {
            int temp = sum - arr[i];

            // checking for condition
            if (temp >= 0 && s.contains(temp))
            {
                /*System.out.println("Pair with given sum " +
                        sum + " is (" + arr[i] +
                        ", "+temp+")");*/
                count++;
            }
            s.add(arr[i]);
        }
        System.out.println(count);
    }

    // Main to test the above function
    public static void main (String[] args)
    {

        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int sum = in.nextInt();
        int[] massive = new int[N];
        for (int i = 0; i < N; i++) {
            massive[i] = in.nextInt();
        }
        /*int massive[] = {2, 5, 1, 3, 4};
        int sum = 6;*/
        amountPairs(massive,  sum);
    }
}