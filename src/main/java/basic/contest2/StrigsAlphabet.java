package basic.contest2;

import java.util.ArrayList;
import java.util.Scanner;

public class StrigsAlphabet {
    public static void main(String[] args) {

       /*String s = "abcdef";*/
       Scanner in = new Scanner(System.in);
       String myInputString = in.nextLine();

       char[] input = myInputString.toCharArray();
        int index = 0;
        int indexBack = input.length-1;

        while(index <= input.length-1){
            if (index%2 == 0){
                System.out.print(input[index]);
            }
            else if(index%2 != 0 && indexBack%2 != 0){
                System.out.print(input[indexBack]);
                indexBack = indexBack -2;
            }
            else if( index%2 != 0 && indexBack%2 == 0){
                indexBack--;
                System.out.print(input[indexBack]);
                indexBack = index;
            }
            index++;
        }
    }
}
