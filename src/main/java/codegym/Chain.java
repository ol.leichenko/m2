package codegym;

import java.util.Scanner;

public class Chain {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int [] res = new int[Math.max(N, 3)];
        res[0] = 1;
        res[1] = 2;
        res[2] = 4;
        for (int i = 3; i < N; i++) {
            res[i] = res[i-1] + res[i-2] + res[i-3];
        }
        System.out.println(res[N-1]);

    }

}