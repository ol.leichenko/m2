package codegym;

import java.util.*;

public class ArrayListSortMy {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            list.add(in.nextInt());
        }
       /* Collections.sort(list,new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o1.compareTo(o2);
                    }
                });*/

        Collections.sort(list, (n1, n2) -> {
            if(n1 % 2 == 0 && n2 % 2 != 0){
                return -1;
            }
            else if(n1 % 2 != 0 && n2 % 2 == 0){
                return 1;
            }
            else if(n1 % 2 == 0 && n2 % 2 == 0){
                return n1.compareTo(n2);
            }
            else if(n1 % 2 != 0 && n2 % 2 != 0){
                return n2.compareTo(n1);
            }


            return 0;

        });

        System.out.println(list);



    }

}