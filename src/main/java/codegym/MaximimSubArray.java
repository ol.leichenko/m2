package codegym;

import java.util.Scanner;

public class MaximimSubArray {

    public static void main(String[] args) {

        String s = "7\n" +
                "-3 4 -1 -1 3 -4 1";
        Scanner in = new Scanner(s);
        int N = in.nextInt();
        int [] arr = new int[N];

        for (int i = 0; i < N; i++) {
                arr[i] = in.nextInt();
        }
        int sum = 0;
        int maxSum = arr[0];
        for (int i = 0; i < N; i++) {
            sum += arr[i];
            maxSum = Math.max(sum, maxSum);
            if (sum < 0) {
                sum = 0;
            }
        }
        System.out.println(maxSum);
        }
    }

