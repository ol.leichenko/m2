package codegym;

import java.util.*;

public class IsConnected {

    public static class DS{
        public int[] joints;

        DS(int N) {
            joints = new int[N];
            for (int i = 0; i < N; i++) {
                joints[i] = i;
            }
        }

        public void union(int i, int j){
            int root = Math.min(root(i),root(j));

            joints[root(i)] = root;
            joints[root(j)] = root;
        }

        public boolean find(int i, int j){
            return (root(i) == root(j));
        }

        public int root(int i) {
            int k = i;
            while( k != joints[k]) {
                k = joints[k];
            }
            return k;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        DS ds = new DS(N);
        for(int i = 0; i < M; i++){
            ds.union(in.nextInt(),in.nextInt());
        }

        int L = in.nextInt();
        for(int i = 0; i < L; i++){
            System.out.println(ds.find(in.nextInt(),in.nextInt())?"connected" : "not connected");
        }
    }
}