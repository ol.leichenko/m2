package codegym;

import java.util.Scanner;

public class DecryptionMy {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        String number = in.next();
        int count = 1;
        for (int i = 2; i < N; i++) {
            int first = Integer.valueOf(number.substring(i-2, i));
            int second = Integer.valueOf(number.substring(i-1, i));
            if (second != 0){
                count++;
            }
            if (first <= 26){
                count++;
            }
        }
        System.out.println(count);

    }

}