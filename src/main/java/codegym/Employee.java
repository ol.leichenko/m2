package codegym;

import java.util.*;

public class Employee{

    private final int salary;
    private final int id;


    Employee (int salary, int id){

        this.salary = salary;
        this.id = id;
    }

     int getSalary(){
        return this.salary;

    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int N = in.nextInt();

        List<Employee> employees = new ArrayList<>();

        for (int idx = 0; idx < N; idx++) {

            int salary = in.nextInt();
            Employee e = new Employee(salary, idx);
            employees.add(e);

        }
        Collections.sort(employees, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.salary - o2.salary;
            }
        });

        for (Employee id: employees) {
            System.out.print(id.id + " ");

        }

        // System.out.println(Arrays.toString(employees.toArray()));



    }

}
