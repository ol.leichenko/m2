package codegym;

import java.util.Scanner;

public class MinimalRoute {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] matrix = new int[N][M];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                matrix[i][j] = in.nextInt();
            }
        }
        int[][] resultMatrix = new int [N][M];
        resultMatrix[0][0] = matrix[0][0];
        for (int j = 1; j < M; j++) {
                resultMatrix[0][j] = resultMatrix[0][j-1] + matrix[0][j];
        }
        for (int i = 1; i < N; i++) {
            resultMatrix[i][0] = resultMatrix[i-1][0] + matrix[1][0];
            for (int j = 1; j < M; j++) {
                resultMatrix[i][j] = matrix[i][j] + Math.min(resultMatrix[i-1][j], resultMatrix[i][j-1]);
            }
        }
        System.out.println(resultMatrix[N-1][M-1]);
    }

}